class Ahorcado{

    constructor(){
        this.palabras = ["Afganistan","Albania","Angola","Bahamas","Belgica","Chipre","Dinamarca","Eqipto", ];
        this.ayudas = ["Su capital es Kabul","Su capital es Tirana","Su capital es Luanda","Su capital es Nassau","Su capital es Bruselas ","Su capital es Nicosia","Su capital es Copenhague","Su capital es El Cairo",  ]
        this.poPalabra;
        this.palabra;
        this.ayuda;
        this.abc;
        this.cont = 6;
        this.generarPalabra();
        this.dibujarTablero();
        this.dibujarLetras();
        this.dividirPalabra();
    }

    generarPalabra(){
        let n;
        n=(Math.random() * (24 - 0)) + 0;
        n=Math.round(n);
        this.poPalabra = n;
        return this.pPalabra;   
    }

    dividirPalabra(){
        this.palabra = this.palabras[this.poPalabra];
        this.palabra = this.palabra.split("");
        document.getElementById('nletras').innerHTML=this.palabra.length; 
        console.log(this.palabra);
        return this.palabra; 

    }

    dibujarTablero() {

        let miTablero = document.getElementById("tablero");

        for (let i = 0; i < this.palabras[this.poPalabra].length; i++) {
            miTablero.innerHTML = miTablero.innerHTML + "<input type='text' id='contenido"+(i)+"' class='contenido' value='' style='color: transparent;text-shadow: 0 0 0 black;'>";
        }
        console.log(this.palabras[this.poPalabra]);
    }

    ayudar(){
        this.ayuda = this.ayudas[this.poPalabra];
        document.getElementById('pista').innerHTML=this.ayuda; 

    }

    dibujarLetras(){
    
        let abecedario = document.getElementById("abecedario");
        let i = "a".charCodeAt(0);
        let j = "z".charCodeAt(0);
        let Ñ = String.fromCharCode("ñ".charCodeAt(0));
        for ( ; i<= j; i++){
            this.abc = String.fromCharCode(i);
            abecedario.innerHTML = abecedario.innerHTML +"<input type='button' id='letra"+this.abc+"' value='"+this.abc+"' onclick='jugar.intento()' class='letra' style='color: transparent;text-shadow: 0 0 0 black;' ></input>"
        }
        abecedario.innerHTML = abecedario.innerHTML +"<input type='button' id='letra"+Ñ+"' value='"+Ñ+"' onclick='jugar.intento()' class='letra' style='color: transparent;text-shadow: 0 0 0 black;' ></input>"
    }

    intento(){
        
        let miElemento=event.target;
        if (this.palabra.indexOf(miElemento.value)!= -1) {
            for (let i = 0; i<= this.palabra.length; i++){
                let n = document.getElementById("contenido"+i)
                if (this.palabra[i]===miElemento.value){
                    n.value = miElemento.value;
                }
            }
        } else {
            this.cont --;
            document.getElementById('intentos').innerHTML="Quedan "+this.cont+" intentos"; 
        }   
    
    
        


    }



}   